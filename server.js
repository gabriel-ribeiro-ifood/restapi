// BASE SETUP

// Chama os pacotes necessários
var express     = require('express'),
    app         = express(),
    bodyParser  = require('body-parser'),
    mongoose    = require('mongoose'),
    mongoURL    = 'mongodb://gabriel:123123@proximus.modulusmongo.net:27017/a3Jymyji',
    Produto     = require('./models/produto');

// Conecta com nossa base de dados
mongoose.connect(mongoURL);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Define nossa porta
var port = process.env.PORT || 8080;


// ROTAS
var router = express.Router();

// Middleware usado para todos requests
router.use(function(req, res, next){
  console.log('passou pelo middleware');

  // Mantém a certeza que iremos para as proximas rotas e nao paramos por aqui
  next(); 
});

// Pequeno teste para verificar se a rota está funcionando
// http://localhost:8080/api
router.get('/', function(req, res){
  res.json({ message: 'bem-vindo a minha API' });
});

// Nas rotas que terminam em /produtos
router.route('/produtos')
  
  // Cria um produto (POST)
  .post(function(req, res){

    var produto = new Produto();

    produto.nome = req.body.nome;
    produto.siteURL = req.body.siteURL;
    produto.preco = req.body.preco;

    produto.save(function(err){
      if (err)
        res.send(err);

      res.json({message: 'Salvamos o produto!'});
    });
  })

  // Recupera os produtos
  .get(function(req, res) {

    Produto.find(function(err, produtos){
      if (err)
        res.send(err);

      res.json(produtos);
    });
  });

// Nas rotas que terminam em /produtos/:produto_id
router.route('/produtos/:produto_id')
  
  // Recupera o produto pelo ID
  .get(function(req, res) {
    Produto.findById(req.params.produto_id, function(err, produto){

      if (err)
        res.send(err);

      res.json(produto);
    });
  })

  // Atualiza o produto pelo ID
  .put(function(req, res) {

    Produto.findById(req.params.produto_id, function(err, produto){

      if (err)
        res.send(err);

      produto.nome = req.body.nome;

      produto.save(function(err){
        if (err)
          res.send(err);

        res.json({message: 'produto atualizado!'});
      });
    });
  })

  .delete(function(req, res){
    Produto.remove({
      _id: req.params.produto_id
    }, function(err, produto){
      if (err)
        res.send(err);

      res.json({ message: 'deletou o produto!'});
    });
  });



// INDICANDO NOSSAS ROTAS
app.use('/api', router);


// INICIA O SERVER
app.listen(port);
console.log('A mágica acontece na porta ' + port);



// Tutorial by Scotch.io
// https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4