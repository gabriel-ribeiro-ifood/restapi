var mongoose  = require('mongoose')
    Schema    = mongoose.Schema;

var ProdutoSchema =  new Schema({
  nome: String,
  siteURL: String,
  preco: String
});

module.exports = mongoose.model('Produto', ProdutoSchema);